package hello.world;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int eggsAmount;
        int eggsMin = 1;
        int milkAmount;
        int milkMin = 200; //milliliter
        int flourAmount;
        int flourMin = 100; //grams

        //System.out.println("Hello [put name here]");
        System.out.println("Blinmaker is starting up..");
        System.out.println("how many eggs do you have?");

        Scanner userInput;
        userInput = new Scanner(System.in);
        eggsAmount =   userInput.nextInt();
        //System.out.println("You have " + eggsAmount + " eggs");
        //eggs done

        System.out.println("how much milk do you have? (ml)");
        userInput = new Scanner(System.in);
        milkAmount =   userInput.nextInt();
        //System.out.println("You have " + milkAmount + " ml milk");
        //milk done

        System.out.println("how much flour do you have?(grams)");
        userInput = new Scanner(System.in);
        flourAmount =   userInput.nextInt();
        //System.out.println("You have " + flourAmount + " g flour");

        //main code here

        if (eggsAmount < eggsMin || milkAmount < milkMin || flourAmount < flourMin){
            System.out.println("no blin today :(");
        } else {
            //math
            flourAmount = flourAmount / flourMin;
            //System.out.println("you have " + flourAmount + "portions of flower");

            milkAmount = milkAmount / milkMin;
           // System.out.println("you have " + milkAmount + "portions of milk");

            //find smallest number of all 3
            /*int smallest;
            if (eggsAmount < milkAmount && milkAmount <= flourAmount){
                smallest =  eggsAmount;

            } else  if(milkAmount <= flourAmount && milkAmount <= eggsAmount){
                smallest =  milkAmount;
            } else{
                smallest =  flourAmount;

            }*/

            int smallest = Math.min(eggsAmount, Math.min(milkAmount, flourAmount));

            System.out.println(" "); //each portion is 4 blins
            System.out.println("You can make " + smallest*4 + " blins");
            System.out.println(" ");
            System.out.println("You will need " + smallest*eggsMin + " eggs");
            System.out.println("You will need " + smallest*flourMin + "grams flour");
            System.out.println("You will need " + smallest*milkMin + "ml milk");
            System.out.println("");
            System.out.println("Blinmaker shutting down..");


        }



    }

}
